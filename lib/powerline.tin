#CLASS {RL_POWERLINE} {kill};
#CLASS {RL_POWERLINE} {open};

#NOP
#NOP     Esta clase pinta todas las barras auxiliares, crea una zona para el mapa, y lo inicia.
#NOP

#ACTION {Abandonas tu estado de concentración bañada en un sudor frío, descubriendo heridas de las que no te habías percatado.} {#var stats[misc] {<jade>desconcentrada};};
#ACTION {Abandonas tu estado de $} {#var stats[misc] {<129><jade>desconcentrada<099>};};
#ACTION {^Cierras los ojos y dejas reposar tus músculos para concentrarte en$} {#var stats[misc] {<129><lime>Concentrada<099>};};
#ACTION {%w, el Gran %2 %w .$} {#var invoca {<fca>%2};};
#SUBSTITUTE {%w, el Gran {Oso|Halcón|Lobo} {Ancestral|Vigilante} .$} {<azure>%w, el Gran {Oso|Halcón|Lobo} {Ancestral|Vigilante}$};

#VAR {stats[health]} {0};
#VAR {stats[maxHealth]} {0};
#VAR {stats[energy]} {0};
#VAR {stats[maxEnergy]} {0};
#VAR {stats[heartbeat]} {0};
#VAR {stats[misc]} {  efectos  };
#VAR {stats[info]} {  Reinos de Leyenda  };
#VAR {buffers[images]} {0};
#VAR {buffers[stoneLayers]} {0};
#SCREEN {get} {cols} {ancho};
#SCREEN {get} {rows} {largo};

#FUNCTION {heartbeatToString} {
    #SWITCH {$stats[heartbeat]} {
        #CASE {0} {#RETURN <G06> <108> ❤ ❤ ❤ ❤  <099>};
        #CASE {1} {#RETURN <G06> <118> ❤ <108>❤ ❤ ❤  <099>};
        #CASE {2} {#RETURN <G06> <118> ❤ ❤ <108>❤ ❤  <099>};
        #CASE {3} {#RETURN <G06> <118> ❤ ❤ ❤ <108>❤  <099>};
        #CASE {4} {#RETURN <G06> <118> ❤ ❤ ❤ ❤  <099>};
    };
};

#ALIAS ticker
{
    #VAR {stats[heartbeat]} {4};
    #TICK {hb} {
        #MATH {stats[heartbeat]} {$stats[heartbeat]+1};
        #IF {$stats[heartbeat] > 4} {
            #VAR {stats[heartbeat]} {0};
        };
        #LINE ignore {#SHOWME {@powerline{}} {-2}};
    } {0.7};
    #TICK {topbar} {
        #LINE IGNORE {#SHOWME {@topbar{}} {1}};
    } {0.8};
    #SPL 2 2 0 0;
    #LINE ignore {#SHOWME {@powerline{}} {-2}};
};

#ALIAS {mapaoff} { #untick pintamapa };

#ALIAS {mapaon} {#TICK {pintamapa} { #LINE ignore {#draw <acf> BOXED MAP 2 120 -3 -1} } {0.92}; #MAP SYNC gmcp.map; #MAP goto };

#EVENT {SESSION CONNECTED} {
     #IGN EVENT ON;
     #VAR {stats[heartbeat]} {4};
     #TICK {hb} {
         #MATH {stats[heartbeat]} {$stats[heartbeat]+1};
         #IF {$stats[heartbeat] > 4} {
             #VAR {stats[heartbeat]} {0};
         };
         #LINE ignore {#SHOWME {@powerline{}} {-2}};
     } {0.7};
#NOP     #TICK {pintamapa} { #LINE ignore {#draw <acf> BOXED MAP 2 120 -3 -1};  } {0.92};
     #TICK {topbar} {
         #LINE IGNORE {#SHOWME {@topbar{}} {1}};
     } {0.8};
     #MAP SYNC gmcp.map;
     #MAP goto;
     #SPL 2 2 0 0;
     #IGN EVENT OFF;
     #UNEVENT {SESSION CONNECTED};
 };

#FUNCTION {topbar}
{
   #NOP #va {$stats[info]} {$pj};
   #FORMAT {info} {<138><afc>%s} {$stats[info]};
   #FORMAT {infoLength} {%L} {$info};
   #FORMAT {barrita} {<cfa> %s <fac>%s <cfa>%s} {~#  } {$info} {  #~};
   #RETURN {@center{$barrita}};
};

#FUNCTION {powerline}
{
    #FORMAT {health} {<ABA><138> PV <afc>%s/%s <099>} {$stats[health]} {$stats[maxHealth]};
    #FORMAT {energy} {<ABE><138> PE <eef>%s/%s <099>} {$stats[energy]} {$stats[maxEnergy]};

    #FORMAT {tgt} {<G09><acf> Caza: <fac>%s <099> } {$target};

    #FORMAT {heartbeatString} {@heartbeatToString{}};

    #IF {$buffers[images] > 0} {
        #FORMAT {images} {<G06><acf><faf> %s imagenes <099>} {$buffers[images]};
    };
    #ELSE {
        #FORMAT {images} {};
    };
    #IF {$buffers[stoneLayers] > 0} {
        #FORMAT {stoneLayers} {<G06><acf><ffa> %s pieles <099>} {$buffers[stoneLayers]};
    };
    #ELSE {
        #FORMAT {stoneLayers} {};
    };

    #FORMAT {misc} {<G06><acf> <acf>%s <099>} {$stats[misc]};
    #FORMAT {miscLength} {%L} {$misc};
    #FORMAT {tgtLength} {%L} {$tgt};
    #FORMAT {healthLength} {%L} {$health};
    #FORMAT {energyLength} {%L} {$energy};
    #FORMAT {heartbeatStringLength} {%L} {$heartbeatString};
    #FORMAT {imagesLength} {%L} {$images};
    #FORMAT {stoneLayersLength} {%L} {$stoneLayers};
    #FORMAT {invocaLength} {%L} {$invoca};

    #MATH {fillerLength} {$ancho - $miscLength - $healthLength - $energyLength - $heartbeatStringLength - $tgtLength - $imagesLength - $invocaLength - $stoneLayersLength};
    #FORMAT {filler} {<G05>%+${fillerLength}s<099>} {};

    #FORMAT {barra} {%s%s%s%s%s%s%s%s%s} {$health} {$energy} {$heartbeatString} {$filler} {$misc} {$images} {$invoca} {$stoneLayers} {$tgt};

    #RETURN {$barra};
};

#ACTION {^Pvs: %1(%2)  Pe: %3(%4)  Po: %5  Xp: %6  Psoc: %7$} {
    #VAR {stats[health]} {%1};
    #VAR {stats[maxHealth]} {%2};
    #VAR {stats[energy]} {%3};
    #VAR {stats[maxEnergy]} {%4};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#ACTION {^Puntos de Vida %* (%1/%2) %*$}
{
    #VAR {stats[health]} {%1};
    #VAR {stats[maxHealth]} {%2};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#ACTION {^Puntos de Energía %* (%1/%2) %*$}
{
    #VAR {stats[energy]} {%1};
    #VAR {stats[maxEnergy]} {%2};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#ACTION {^Pvs: %1(%2)  Pe: %3(%4)  Fe: %5(%6)  Po: %7  Xp: %8  Psoc: %9$} {
    #VAR {stats[health]} {%1};
    #VAR {stats[maxHealth]} {%2};
    #VAR {stats[energy]} {%3};
    #VAR {stats[maxEnergy]} {%4};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};
#ACTION {^Pvs: %1\/%2 (%3) Pe: %4\/%5 (%6)$} {

    #MATH {healthDelta} {%1 - $stats[health]};
    #VAR {stats[health]} {%1};
    #VAR {stats[maxHealth]} {%2};
    #VAR {stats[energy]} {%4};
    #VAR {stats[maxEnergy]} {%5};
    #SHOWME {Δ @deltaColor{$healthDelta}$healthDelta<099>};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};
#ACTION {^Pvs: %1\/%2 Pe: %3\/%4$} {
    #MATH {healthDelta} {%1 - $stats[health]};
    #VAR {stats[health]} {%1};
    #VAR {stats[maxHealth]} {%2};
    #VAR {stats[energy]} {%3};
    #VAR {stats[maxEnergy]} {%4};
    #SHOWME {Δ @deltaColor{$healthDelta}$healthDelta<099>};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};
#ACTION {^%1 imagenes semejantes a ti aparecen a tu alrededor.$} {

    #VAR {buffers[images]} {@parseNumber{%1}};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#ACTION {^Estás rodead{o|a} por %1 im{a|á}genes id{é|e}nticas a ti.$} {
    #VAR {buffers[images]} {%1};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#NOP TODO: Integrate with above via | -Shaktty
#ACTION {^Estás rodeada por 1 im{á|a}gen id{é|e}ntica a ti.$} {
    #VAR {buffers[images]} {-1};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#NOP fixed by taking out the ^ and avoiding the # altogether -shaktty
#ACTION {Una de tus im{a|á}genes desaparece cuando %* la golpea.$} {
    #MATH {buffers[images]} {$buffers[images] - 1};
    #IF {$buffers[images] < 0} {
        #VAR {buffers[images]} 0;
    };
   @message{"1 Imagen menos"};
  #LINE IGNORE    #SHOWME {@powerline{}} {-2};
};

#ACTION {^¡Tus im{a|á}genes se desvanecen!$} {
    #VAR {buffers[images]} 0;
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
} {4};

#ACTION {^Tu piel queda cubierta por %1 fin{i|í}simas, pero resistentes, capas de piedra.$} {
    #VAR {buffers[stoneLayers]} {%1};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#ACTION {^* El ataque de %* rebota en tu piel de piedra.$} {
    #MATH {buffers[stoneLayers]} {$buffers[stoneLayers] - 1};
    #IF {$buffers[stoneLayers] < 0} {
        #VAR {buffers[stoneLayers]} 0;
    };
  #LINE ignore  #SHOWME {@pullRight{<BC11><FFFF> 1 piel menos !!}};
#LINE IGNORE #SHOWME {@powerline{}} {-2};
}

#ACTION {^Tu piel vuelve a su estado normal.$} {
    #VAR {buffers[stoneLayers]} 0;
  #LINE ignore  #SHOWME {@center{" <acf> PIELES OUT !! PIELES OUT !!"}};
    #LINE IGNORE #SHOWME {@powerline{}} {-2};
} {4};

#NOP Added regeneration calculations -Shaktty
#ACTION {^Tu piel de piedra se endurece y fortalece m{á|a}gicamente, haciendo que tu total de capas de piedra suba a %1.$} {
    #VAR {buffers[stoneLayers]} {%1};
 #LINE IGNORE #SHOWME {@powerline{}} {-2};
};
#ACTION {^Tus clones ilusorios se dividen y reorganizan, haciendo que tu total de im{a|á}genes suba a %1.$} {
   #VAR {buffers[images]} {%1};
#LINE IGNORE #SHOWME {@powerline{}} {-2};
};

#CLASS {RL_POWERLINE} {close};
